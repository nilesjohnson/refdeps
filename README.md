
# DESCRIPTION


## refdeps.sty

Latex package to make a graph of reference dependencies.

This package defines markup commands for your tex source, and then
uses those when you compile to produce the graph.  The graph data is
stored in two files, `vertices.xrf` and `edges.xrf`.  These are loaded
by the file `refdeps.html` to construct the graph.  The html file
depends on javascript and css files located in the `js_css` directory.

### usage
Because this package has many associated files, the cleanest way to
use it is to put the entire `refdeps` directory into the same
directory as your tex project.  Then load the package with


    \usepackage{refdeps/refdeps}


in your tex file.


### modifying graph parameters

If your graph is complex, you may wish to modify the force parameters
that shape the graph.  These include edge length and negative
attraction of vertices (charge).  These are set in a code
block in `refdeps.html` which is something like the following.

```javascript
// Adjust force parameters below to suit your taste
// Set up the force layout. But don't start yet!
linkramp=d3.scale.linear().domain([0,1,4]).range([60,90,150]).clamp(true)
var force = d3.layout.force()
    .size([width, height])
    .linkDistance(90)
    //.linkDistance(function (d) {return (50 - 5*(d.target.page - d.source.page))})
    //.charge(function (d) {return -10*d.page^2})
    .charge(-400)
    .gravity(0.1);
```


## extras

These files are not needed for the package to function, but may be
useful to you.

### extras/add_proofrefs.py

This is a script to add `\proofof` commands to tex files.  It does
this by scanning the file to detect the current active label, and add
`\proofof` whenever it finds a bare `\begin{proof}`, using the current
active label.  This mostly works, but is prone to a couple of likely
failures:

  - If the statement of the theorem (or prop, lem, cor) doesn't have a
    label, the `\proofof` will use whatever label is current --
    probably the wrong one.
  - If the statement contains *another* label, such as labeled items,
    the `\proofof` will use whatever label is current -- probably the
    wrong one.  This could be fixed by only setting the active label
    when a theoremlike environment is detected.

WARNING: this script modifies the input file, and hasn't been tested
very much.  Definitely don't use it without making a backup copy of
your files, or without a tool to track and review the changes it
makes.


### extras/demo_*

There are several demonstrations of the graphs produced, so you can
see how they work.

# LICENSE

This work is licensed under the GNU General Public License v3.0.  See
LICENSE.txt for details.


# CHANGELOG


## Based on part of www-textools
  
Willie Wong https://gitlab.msu.edu/u/wongwil2

https://gitlab.msu.edu/wongwil2/www-textools

This implements two major features over www-textools:

  - useable as a latex package (.sty file)
  - generates json instead of csv, and therefore can be used 
    without a webserver


## External libraries

This package uses parts of the jquery and d3 javascript libraries; the
necessary parts are included in the `js_css` directory.

  - https://jquery.com/ (MIT license)
  - https://d3js.org/ (BSD license)
