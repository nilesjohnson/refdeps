"""
python script to add \proofof{...} commands to your tex file
don't run this if you don't have a way of verifying the changes are correct

add_refs(filename):
Reads line by line, detecting \label commands and \begin commands

  - if \label, store the argument as currentlabel

  - if \begin is an environment which could have a proof 
    (see THM_ENVS), then print that line and the following one

  - if \begin is proof, with nothing else following it, then add 
    \proofof{currentlabel}

  - if \begin is proof, with other text on the same line, print 
    'EXCEPTION' and do not change the line (do these by hand)

By default, add_refs prints this output, but doesn't actually make changes.
Use add_refs(filename,do_replace=True) to actually change the file.
"""

import re

THM_ENVS = ['thm','prop','cor','lem']

re_begin = re.compile(r'.*\\begin{(.+?)}(.*)')
re_begin_proof = re.compile(r'\\begin{proof}'+".*$")
re_label = re.compile(r'.*\\label{(.+?)}.*')


def format_status(n,msg,txt):
    """
    formats line number, message, and 45 chars of line text
    """
    return "l. {0:06d} {1} : {2}".format(n,msg.rjust(13),txt[0:45])

def add_refs(FILENAME,do_replace=False):
    """
    set do_replace=True to actually do the replacement
    """
    with open (FILENAME, 'r') as f:
        lines = f.readlines()

    currentlabel = ''
    for i in range(len(lines)):
        line = lines[i]
        n = re_label.match(line)
        if n:
            currentlabel = n.group(1)
            #print currentlabel

        b = re_begin.match(line)
        if b and b.group(1) in THM_ENVS:
            print ''
            for j in [0,1]:
                templine = lines[i+j]
                tm = re_label.match(templine)
                if tm:
                    msg = '--label-->'
                else:
                    msg = ''
                print format_status(i+j+1,msg,templine[:-1]) #remove newline, print first 60 chars
        if b and b.group(1) == 'proof':
            if b.group(2) != '':
                msg = '**EXCEPTION**'
            else:
                msg = '*new* proofof'
                line = line[:-1]+r'\proofof{'+currentlabel+'}'+line[-1] #last character is newline
                lines[i] = line
            print format_status(i+1,msg,lines[i][:-1]) #last character is newline
                

    if do_replace:
        with open (FILENAME, 'w') as f:
            f.writelines(lines)
    else:
        print ""
        print "NOTE"
        print "do_replace=False, therefore not replacing lines in file"

def add_labels():
    pass
